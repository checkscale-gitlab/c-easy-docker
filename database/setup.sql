SET NAMES utf8;

DROP DATABASE IF EXISTS CEASY;
CREATE DATABASE CEASY;

USE CEASY;

DROP TABLE IF EXISTS `CHAPTER`;
CREATE TABLE `CHAPTER` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `title` varchar(255) NOT NULL,
  `order` int(11) DEFAULT -1,
  PRIMARY KEY (`id`)
) CHARACTER SET utf8;
 
DROP TABLE IF EXISTS `COURSE`;
CREATE TABLE `COURSE` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `idChapter` int(11) NOT NULL,
  `title` varchar(255) NOT NULL,
  `content` varchar(255),
  `order` int(11) DEFAULT -1,
  PRIMARY KEY (`id`),
  CONSTRAINT `courseFk` FOREIGN KEY (`idChapter`) REFERENCES `CHAPTER` (`id`) ON DELETE CASCADE
) CHARACTER SET utf8;
 
DROP TABLE IF EXISTS `EXERCISE`;
CREATE TABLE `EXERCISE` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `idCourse` int(11) NOT NULL,
  `type`int(1) DEFAULT 0,
  `title` varchar(255) NOT NULL,
  `instruction` text DEFAULT NULL,
  `correction` text DEFAULT NULL,
  `dependency` text DEFAULT NULL,
  `main` text DEFAULT NULL,
  `order` int(11) DEFAULT -1,
  PRIMARY KEY (`id`),
  CONSTRAINT `exerciseFk` FOREIGN KEY (`idCourse`) REFERENCES `COURSE` (`id`) ON DELETE CASCADE
) CHARACTER SET utf8;
 
DROP TABLE IF EXISTS `TESTSET`;
CREATE TABLE `TESTSET` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `idExercise` int(11) NOT NULL,
  `input` varchar(255) DEFAULT NULL,
  `expected` varchar(255) DEFAULT NULL,
  PRIMARY KEY (`id`),
  CONSTRAINT `testSetFk` FOREIGN KEY (`idExercise`) REFERENCES `EXERCISE` (`id`) ON DELETE CASCADE
) CHARACTER SET utf8;
 
DROP TABLE IF EXISTS `USER`;
CREATE TABLE `USER` (
  `id` varchar(11) NOT NULL,
  `name` varchar(255) NOT NULL,
  `password`varchar(255) NOT NULL,
  `type` int(11) DEFAULT '0',
  `promo` int(11) DEFAULT NULL,
  PRIMARY KEY (`id`)
) CHARACTER SET utf8;

INSERT INTO `USER` VALUES ('admin','Amine ISTRATEUR','$argon2i$v=19$m=4096,t=3,p=1$P2uC8TaEGWaWhRrMfweCsA$lsRh6Hbl0zkQVBTPbXBkBaJ9CY0BaPWx0vJ6VbuX4+k',1, 1);
 
DROP TABLE IF EXISTS `DO`;
CREATE TABLE `DO` (
  `idUser` varchar(11) NOT NULL,
  `idExercise` int(11) NOT NULL,
  `startTime` datetime DEFAULT NULL,
  `completionTime` datetime DEFAULT NULL,
  `try` int(11) DEFAULT '0',
  `done` int(1) DEFAULT '0',
  `code` text DEFAULT NULL,
  PRIMARY KEY (`idUser`, `idExercise`),
  CONSTRAINT `doFk1` FOREIGN KEY (`idUser`) REFERENCES `USER` (`id`) ON DELETE CASCADE,
  CONSTRAINT `doFk2` FOREIGN KEY (`idExercise`) REFERENCES `EXERCISE` (`id`) ON DELETE CASCADE
) CHARACTER SET utf8;
