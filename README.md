# C-Easy Docker

Ce repository contient les containers Docker permettant de simuler l'architecture de C-Easy. Avant d'exécuter la commande de déploiement, merci de lire les [Consignes de C-Easy Server](https://gitlab.com/c-easy/c-easy-server#consignes).

## Déployer

Pour lancer les serveurs, il suffit d'utiliser la commande en étant à la racine du projet : 
```
docker-compose up --build -d
```

Pour éteindre le serveurs, il faudra utiliser la commande :
```
docker-compose down
```